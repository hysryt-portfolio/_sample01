    <footer class="footer">
      <div class="footer__inner">
        <div class="footer-main">
          <div class="footer-main__left">
            <div class="footer-info">
              <div class="footer-info__brand">
                <p class="footer-info__logo">
                  <img src="<?= get_template_directory_uri(); ?>/img/common/logo_white.svg" alt="">
                </p>
                <p class="footer-info__name">pencil</p>
              </div>
              <p class="footer-info__address">
                〒509-0000<br>
                岐阜県瑞浪市○○町1-2-3<br>
                <a href="http://maps.google.com/maps?q=瑞浪駅" target="_blank">Google Mapで開く<i class="fas fa-external-link-alt"></i></a>
              </p>
            </div>
          </div>
          <div class="footer-main__right">
            <div class="footer-nav">
              <ul class="footer-nav__list">
                <li class="footer-nav__list-item"><a href="<?php bloginfo('url'); ?>/items">取扱商品</a></li>
                <li class="footer-nav__list-item"><a href="<?php bloginfo('url'); ?>/gallery">ギャラリー</a></li>
                <li class="footer-nav__list-item"><a href="<?php bloginfo('url'); ?>/blog">ブログ</a></li>
                <li class="footer-nav__list-item"><a href="<?php bloginfo('url'); ?>/qa">Q&amp;A</a></li>
                <li class="footer-nav__list-item"><a href="<?php bloginfo('url'); ?>/recruit">採用情報</a></li>
              </ul>
              <div class="footer-nav__contacts">
                <p class="footer-nav__tel">
                  <i class="fas fa-phone"></i>123-4567
                </p>
                <p class="footer-nav__mail">
                  <a href="<?php bloginfo('url'); ?>/contact"><i class="fas fa-envelope"></i>お問い合わせ</a>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-copyright">
          Copyright(c) <?php bloginfo('name'); ?> All right reserved.
        </div>
      </div>
    </footer>
    <?php wp_footer(); ?>
  </body>
</html>