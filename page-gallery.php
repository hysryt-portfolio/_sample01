<?php get_header(); ?>

<main>
  <div class="sub-header">
    <div class="sub-header__inner">
      <h2 class="sub-header__title">ギャラリー</h2>
    </div>
  </div>

  <div class="gallery-list">
    <div class="gallery-list__inner">
      <p class="gallery-list-item">
        <a href="<?= get_template_directory_uri(); ?>/img/gallery/origin/01.jpg" data-lightbox="gallery"><img src="<?= get_template_directory_uri(); ?>/img/gallery/01.jpg" alt=""></a>
      </p>
      <p class="gallery-list-item">
        <a href="<?= get_template_directory_uri(); ?>/img/gallery/origin/04.jpg" data-lightbox="gallery"><img src="<?= get_template_directory_uri(); ?>/img/gallery/04.jpg" alt=""></a>
      </p>
      <p class="gallery-list-item">
        <a href="<?= get_template_directory_uri(); ?>/img/gallery/origin/05.jpg" data-lightbox="gallery"><img src="<?= get_template_directory_uri(); ?>/img/gallery/05.jpg" alt=""></a>
      </p>
      <p class="gallery-list-item">
        <a href="<?= get_template_directory_uri(); ?>/img/gallery/origin/02.jpg" data-lightbox="gallery"><img src="<?= get_template_directory_uri(); ?>/img/gallery/02.jpg" alt=""></a>
      </p>
      <p class="gallery-list-item">
        <a href="<?= get_template_directory_uri(); ?>/img/gallery/origin/03.jpg" data-lightbox="gallery"><img src="<?= get_template_directory_uri(); ?>/img/gallery/03.jpg" alt=""></a>
      </p>
      <p class="gallery-list-item">
        <a href="<?= get_template_directory_uri(); ?>/img/gallery/origin/02.jpg" data-lightbox="gallery"><img src="<?= get_template_directory_uri(); ?>/img/gallery/02.jpg" alt=""></a>
      </p>
      <p class="gallery-list-item">
        <a href="<?= get_template_directory_uri(); ?>/img/gallery/origin/01.jpg" data-lightbox="gallery"><img src="<?= get_template_directory_uri(); ?>/img/gallery/01.jpg" alt=""></a>
      </p>
    </div>
  </div>

</main>

<?php get_footer(); ?>