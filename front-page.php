<?php get_header(); ?>

<main>
  <section class="top-section top-section-concept">
    <div class="top-section-concept__inner">
      <div class="top-section-concept__image"></div>
      <div class="top-section-concept__body">
        <div class="top-concept">
          <h2 class="top-concept__title">気軽に、いつでも。</h2>
          <div class="top-concept__body">
            <p class="top-concept__text">
              地方にだって都会に負けないくらいの品揃えの文房具店が欲しい、<br>
              という思いから pencil は生まれました。
            </p>
            <p class="top-concept__text">
              いろんな文房具を見たいけどそのために都会まで足を運ぶのはちょっと...という<br>
              文房具好きの方々の身近な存在でありたいと考えています。
            </p>
            <p class="top-concept__text">
              どんな方でも、暇つぶしにふらっと立ち寄れるような空間を心がけています。
            </p>
            <p class="top-concept__text">
              当店をきっかけに、文房具への愛をより一層深めていただければ<br>
              我々としてこの上ない喜びです。
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="top-section top-section-new">
    <div class="top-section-new__inner">
      <h2 class="top-section-new__title">新着商品</h2>
      <ul class="top-section-new__list">

        <?php
          $posts = get_posts( [
            'posts_per_page' => 3, // 3件分取得
            'post_type' => 'item',
          ] );
        ?>

        <?php foreach ( $posts as $post ) : setup_postdata( $post ) ?>
          <?php
            $item_image_id = get_post_meta( get_the_ID(), ItemPostType::META_BOX_ITEM_IMAGE, true );
            $item_image_url = wp_get_attachment_image_src( $item_image_id, [300, 300])[0];
            $item_tags = get_the_terms( get_the_ID(), 'item_tag' );
            $item_description = get_post_meta( get_the_ID(), ItemPostType::META_BOX_ITEM_DESCRIPTION, true );
          ?>
          <li class="top-section-new__list-item">
            <a href="<?php bloginfo('url'); ?>/items#item<?= esc_attr( get_the_ID() ); ?>">
              <div class="top-new-item">
                <p class="top-new-item__image">
                  <img src="<?= esc_url( $item_image_url ); ?>" alt="鉛筆">
                </p>
                <h3 class="top-new-item__name"><?php the_title(); ?></h3>
                <ul class="top-new-item__tags">
                  <?php foreach ( $item_tags as $tag ) : ?>
                    <li class="common-tag top-new-item__tag"><?= esc_html( $tag->name ); ?></li>
                  <?php endforeach ?>
                </ul>
                <p class="top-new-item__description">
                  <?= esc_html( $item_description ); ?>
                </p>
              </div>
            </a>
          </li>
        <?php endforeach ?>
        <?php wp_reset_postdata(); ?>

      </ul>
      <p class="top-section-new__button">
        <a href="<?php bloginfo('url'); ?>/items">商品紹介ページ</a>
      </p>
    </div>
  </section>

  <section class="top-section top-section-social">
    <div class="top-section-social__inner">
      <div class="top-section-social__blog">
        <section class="top-section-blog">
          <h2 class="top-section-blog__title">Blog</h2>
          <ul class="top-section-blog__articles">

            <?php
              $posts = get_posts( [
                'posts_per_page' => 5 // 新しい投稿を5件取得
              ] );
            ?>

            <?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
              <li class="top-section-blog__articles-item">
                <a href="<?php the_permalink(); ?>" class="top-blog-article">
                  <p class="top-blog-article__date"><?php the_time("Y/m/d"); ?></p>
                  <p class="top-blog-article__title"><?php the_title(); ?></p>
                </a>
              </li>
            <?php endforeach ?>
            <?php wp_reset_postdata(); ?>

          </ul>
        </section>
      </div>
      <div class="top-section-social__twitter">
        <section class="top-section-twitter">
          <a class="twitter-timeline" data-lang="ja" data-width="300" data-height="400" data-theme="light" data-link-color="#988c7f" href="https://twitter.com/TwitterDev/timelines/539487832448843776?ref_src=twsrc%5Etfw">National Park Tweets - Curated tweets by TwitterDev</a>
          <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </section>
      </div>
    </div>
  </section>

  <section class="top-section top-section-access">
    <h2 class="top-section-access__title">アクセス</h2>
    <div class="top-section-access__map" id="map"></div>
    <script>
      function initMap() {
        var pos = {lat: 35.369022, lng: 137.252081};

        var map = new google.maps.Map(document.getElementById('map'), {
          center: pos,
          zoom: 17,
          styles: [
            {
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#f5f5f5"
                }
              ]
            },
            {
              "elementType": "labels.icon",
              "stylers": [
                {
                  "saturation": -35
                },
                {
                  "visibility": "simplified"
                }
              ]
            },
            {
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#616161"
                }
              ]
            },
            {
              "elementType": "labels.text.stroke",
              "stylers": [
                {
                  "color": "#f5f5f5"
                }
              ]
            },
            {
              "featureType": "administrative.land_parcel",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#bdbdbd"
                }
              ]
            },
            {
              "featureType": "poi",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#eeeeee"
                }
              ]
            },
            {
              "featureType": "poi",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#757575"
                }
              ]
            },
            {
              "featureType": "poi.park",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#e5e5e5"
                }
              ]
            },
            {
              "featureType": "poi.park",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#9e9e9e"
                }
              ]
            },
            {
              "featureType": "road",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#ffffff"
                }
              ]
            },
            {
              "featureType": "road.arterial",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#757575"
                }
              ]
            },
            {
              "featureType": "road.highway",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#dadada"
                }
              ]
            },
            {
              "featureType": "road.highway",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#616161"
                }
              ]
            },
            {
              "featureType": "road.local",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#9e9e9e"
                }
              ]
            },
            {
              "featureType": "transit.line",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#dbdbdb"
                }
              ]
            },
            {
              "featureType": "transit.station",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#eeeeee"
                },
                {
                  "saturation": -40
                }
              ]
            },
            {
              "featureType": "water",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#b4e7f6"
                }
              ]
            },
            {
              "featureType": "water",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#9e9e9e"
                }
              ]
            }
          ]
        });

        new google.maps.Marker({position: pos, map: map});
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYjH2xc8ghs7dCbGwXSg9ZkUbGquG8At4&callback=initMap"
    async defer></script>
    <div class="top-section-access__inner">
      <p class="top-section-access__address">
        〒509-0000<br>
        岐阜県瑞浪市○○町1-2-3<br>
        <a href="http://maps.google.com/maps?q=瑞浪駅" target="_blank">Google Mapで開く<i class="fas fa-external-link-alt"></i></a>
      </p>
    </div>
  </section>

</main>

<?php get_footer(); ?>