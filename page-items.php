<?php get_header(); ?>

<?php
  $posts = get_posts( [
    'posts_per_page' => 50, // 50件分取得
    'post_type' => 'item',
  ] );

  $reverse = false;
?>

<main>
  <div class="sub-header">
    <div class="sub-header__inner">
      <h2 class="sub-header__title">取扱商品</h2>
      <p class="sub-header__subtitle">
        ここにない商品も数多く取り扱っております。<br>
        ぜひお店までお越しください。
      </p>
    </div>
  </div>

  <div class="items-list">
    <div class="items-list__inner">
      <?php foreach ( $posts as $post ) : setup_postdata( $post ) ?>
        <?php
          $item_image_id = get_post_meta( get_the_ID(), ItemPostType::META_BOX_ITEM_IMAGE, true );
          $item_image_url = wp_get_attachment_image_src( $item_image_id, [600, 600])[0];
          $item_tags = get_the_terms( get_the_ID(), 'item_tag' );
          $item_price = get_post_meta( get_the_ID(), ItemPostType::META_BOX_ITEM_PRICE, true );
          $item_description = get_post_meta( get_the_ID(), ItemPostType::META_BOX_ITEM_DESCRIPTION, true );
        ?>

        <section class="items-list-item <?= $reverse ? '_reverse' : '' ?>">
          <a class="items-list-item__anchor" id="item<?= esc_attr( get_the_ID() ); ?>"></a>
          <p class="items-list-item__image">
            <img src="<?= esc_url( $item_image_url ); ?>" alt="">
          </p>
          <div class="items-list-item-info">
            <h3 class="items-list-item-info__title"><?php the_title(); ?></h3>
            <ul class="items-list-item-info__tags">
              <?php foreach ( $item_tags as $tag ) : ?>
                <li class="common-tag items-list-item-info__tag"><?= esc_html( $tag->name ); ?></li>
              <?php endforeach ?>
            </ul>
            <?php if ( $item_price !== "" ) : ?>
              <p class="items-list-item-info__price"><?= esc_html( $item_price ); ?></p>
            <?php endif ?>
            <p class="items-list-item-info__description">
              <?= esc_html( $item_description ); ?>
            </p>
          </div>
        </section>

        <?php $reverse = !$reverse; ?>
      <?php endforeach ?>
    </div>
  </div>
</main>

<?php get_footer(); ?>