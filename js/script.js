(function() {
  if (document.querySelector('.header-bar-hamburger')) {
    setHamburgerButton();
  }

  if (document.querySelector('.header-top')) {
    setToppageAction();
  }

  if (document.querySelector('.qa-block')) {
    setQaAccordion();
  }
})();


/**
 * ヘッダーバーのハンバーガーボタンの設定
 */
function setHamburgerButton() {
  var headerHamburger = document.querySelector('.header-bar-hamburger');
  var headerNav = document.querySelector('.header-bar-nav');
  var headerBrand = document.querySelector('.header-bar-brand');

  var eventName = (window.ontouchstart === undefined) ? "click" : "touchstart";

  headerHamburger.addEventListener("click", function() {
    toggleHamburgerMenu(headerHamburger, headerNav, headerBrand);
  });
}


function toggleHamburgerMenu(headerHamburger, headerNav, headerBrand) {
  var modifier = '_open';

  if (headerHamburger.classList.contains(modifier)) {
    headerHamburger.classList.remove(modifier);
    headerNav.classList.remove(modifier);
    headerBrand.classList.remove(modifier);

  } else {
    headerHamburger.classList.add(modifier);
    headerNav.classList.add(modifier);
    headerBrand.classList.add(modifier);
  }
}


/**
 * トップページの時のヘッダーバーの動きを設定
 */
function setToppageAction() {
  var topHeader = document.querySelector('.header-top');
  var height = topHeader.clientHeight;
  var headerBar = document.querySelector('.header-bar');

  toggleHeaderBar(height, headerBar);
  document.addEventListener('scroll', function() {
    toggleHeaderBar(height, headerBar);
  });
}


/**
 * ヘッダーバーの表示/非表示のトグル処理
 * @param {Number} topHeaderHeight 
 * @param {Element} headerBar 
 */
function toggleHeaderBar(topHeaderHeight, headerBar) {
  if (window.scrollY >= topHeaderHeight && !headerBar.classList.contains('_show')) {
    headerBar.classList.add('_show');

  } else if (window.scrollY < topHeaderHeight && headerBar.classList.contains('_show')) {
    headerBar.classList.remove('_show');
  }
}


/**
 * Q&Aページのアコーディオンを設定
 */
function setQaAccordion() {
  var blocks = document.querySelectorAll('.qa-block');

  Object.keys(blocks).forEach(function(key) {
    var block = blocks[key];
    var question = block.querySelector('.qa-question');
    var answer = block.querySelector('.qa-answer-wrapper');

    if (!question || !answer) {
      return;
    }

    var eventName = (window.ontouchstart === undefined) ? "click" : "touchstart";

    question.addEventListener(eventName, function() {
      toggleQaAccordion(question, answer);
    });
  });
}


/**
 * Q&Aページのアコーディオンの開閉のトグル処理
 * @param {Element} question 
 * @param {Element} answer 
 */
function toggleQaAccordion(question, answer) {
  if (question.classList.contains("_open")) {
    question.classList.remove("_open");
    answer.style.height = "";

  } else {
    question.classList.add("_open");
    var height = getElementAutoHeight(answer);
    requestAnimationFrame(function() {
      answer.style.height = height + "px";
    });
  }
}


/**
 * 要素のheightプロパティがautoの時の高さを取得
 * @param {Element} element 
 */
function getElementAutoHeight(element) {
  element.style.height = "auto";
  var height = element.clientHeight;
  element.style.height = "";
  return height;
}