document.addEventListener('DOMContentLoaded', function() {
  var modal = new wp.media({
    multiple: false
  });

  modal.on('select', function() {
    var attachment = modal.state().get('selection').first().toJSON();
    var imgtag = '<img src="' + attachment.url + '" alt="" />';

    document.querySelector('.media-image').innerHTML = imgtag;
    document.querySelector('.media-id').setAttribute('value', attachment.id);
  });

  document.querySelector('.media-upload-button').addEventListener('click', function() {
    modal.open();
  });
})