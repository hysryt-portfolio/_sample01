<?php

require("class/recruit-admin-page.php");
require("class/item-post-type.php");
require("class/qa-post-type.php");

add_action( 'init', function() {
    // 採用情報ページの管理画面設定
    new RecruitAdminPage();

    // 取扱商品のカスタム投稿タイプ設定
    new ItemPostType();

    // Q&Aのカスタム投稿タイプ設定
    new QaPostType();
} );

// 管理画面用CSSの読み込み
add_action( 'admin_enqueue_scripts', function() {
    wp_enqueue_style( 'my_admin_style', get_template_directory_uri().'/style-admin.css' );
} );