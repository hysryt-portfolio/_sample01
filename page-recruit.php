<?php get_header(); ?>

<main>
  <div class="sub-header">
    <div class="sub-header__inner">
      <h2 class="sub-header__title">採用情報</h2>
    </div>
  </div>

  <section class="recruit-section">
    <h3 class="recruit-section__title">pencilでは一緒に働いてくれる、<wbr>文房具が好きな方を募集しています。</h3>
    <?php
      $recruit_properties = get_post_meta( get_the_ID(), "recruit_props", true);
    ?>
    <div class="recruit-table">
      <?php foreach ($recruit_properties as $prop) : ?>
        <?php
          if ( trim( $prop["name"] ) === "") {
            continue;
          }
        ?>
        <div class="recruit-table__row">
          <p class="recruit-table__header-cell"><?= esc_html( $prop["name"] ); ?></p>
          <p class="recruit-table__cell"><?= nl2br( esc_html( $prop["value"] ) ); ?></p>
        </div>
      <?php endforeach ?>
    </div>
  </section>

</main>

<?php get_footer(); ?>