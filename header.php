<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="robots" content="noindex">
    <title><?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Volkhov" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>?20180711">
    <script defer src="https://use.fontawesome.com/releases/v5.1.0/js/solid.js" integrity="sha384-Z7p3uC4xXkxbK7/4keZjny0hTCWPXWfXl/mJ36+pW7ffAGnXzO7P+iCZ0mZv5Zt0" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.1.0/js/fontawesome.js" integrity="sha384-juNb2Ils/YfoXkciRFz//Bi34FN+KKL2AN4R/COdBOMD9/sV/UsxI6++NqifNitM" crossorigin="anonymous"></script>
    <link href="<?= get_template_directory_uri(); ?>/css/lightbox.min.css" rel="stylesheet">
    <?php if ( is_front_page() ) : ?>
      <script src="<?= get_template_directory_uri(); ?>/js/pace.js"></script>
    <?php endif ?>
    <script defer src="<?= get_template_directory_uri(); ?>/js/script.js"></script>
    <?php wp_head(); ?>
    <script defer src="<?= get_template_directory_uri(); ?>/js/lightbox.min.js"></script>
  </head>
  <body <?php body_class(); ?>>

    <?php
      $headerClass = "";
      $headerBarClass = "";
      if ( ! is_front_page() ) {
        $headerBarClass = "_show";
      } else {
        $headerClass = "_top";
      }
    ?>

    <header class="header <?= $headerClass ?>">

      <div class="header-bar <?= $headerBarClass ?>">
        <div class="header-bar__inner">
          <a href="<?php bloginfo('url'); ?>" class="header-bar-brand">
            <p class="header-bar-brand__image">
              <img src="<?= get_template_directory_uri(); ?>/img/common/logo.svg">
              <img class="header-bar-brand__white-logo" src="<?= get_template_directory_uri(); ?>/img/common/logo_white.svg">
            </p>
            <p class="header-bar-brand__name">
              pencil
            </p>
          </a>
          <div class="header-bar-hamburger">
            <span class="header-bar-hamburger__bar">
          </div>
          <nav class="header-bar-nav">
            <ul class="header-bar-nav__list">
              <li class="header-bar-nav__list-item"><a href="<?php bloginfo('url'); ?>/items">取扱商品<small>items</small></a></li>
              <li class="header-bar-nav__list-item"><a href="<?php bloginfo('url'); ?>/gallery">ギャラリー<small>gallery</small></a></li>
              <li class="header-bar-nav__list-item"><a href="<?php bloginfo('url'); ?>/blog">ブログ<small>blog</small></a></li>
              <li class="header-bar-nav__list-item"><a href="<?php bloginfo('url'); ?>/qa">Q&amp;A<small>Q and A</small></a></li>
              <li class="header-bar-nav__list-item"><a href="<?php bloginfo('url'); ?>/recruit">採用情報<small>recruit</small></a></li>
              <li class="header-bar-nav__list-item"><a href="<?php bloginfo('url'); ?>/contact">お問い合わせ<small>contact</small></a></li>
            </ul>
          </nav>
        </div>
      </div>

      <?php if ( is_front_page() ) : ?>

        <div class="header-top">
          <div class="header-top-nav">
            <div class="header-top-nav__inner">
              <p class="header-top-nav__logo">
                <img src="<?php echo get_template_directory_uri() ?>/img/common/logo.svg" alt="">
              </p>
              <h1 class="header-top-nav__title">pencil</h1>
              <p class="header-top-nav__subtitle">岐阜県瑞浪市にある<wbr>架空の文房具店です。</p>
              <ul class="header-top-nav__list">
                <li class="header-top-nav__list-item"><a href="<?php bloginfo('url'); ?>/items"><i class="fas fa-angle-right"></i>取扱商品</a></li>
                <li class="header-top-nav__list-item"><a href="<?php bloginfo('url'); ?>/gallery"><i class="fas fa-angle-right"></i>ギャラリー</a></li>
                <li class="header-top-nav__list-item"><a href="<?php bloginfo('url'); ?>/blog"><i class="fas fa-angle-right"></i>ブログ</a></li>
                <li class="header-top-nav__list-item"><a href="<?php bloginfo('url'); ?>/qa"><i class="fas fa-angle-right"></i>Q&amp;A</a></li>
                <li class="header-top-nav__list-item"><a href="<?php bloginfo('url'); ?>/contact"><i class="fas fa-angle-right"></i>お問い合わせ</a></li>
                <li class="header-top-nav__list-item"><a href="<?php bloginfo('url'); ?>/recruit"><i class="fas fa-angle-right"></i>採用情報</a></li>
              </ul>
            </div>
          </div>

          <div class="header-top-hero">
            <p class="header-top-hero__text">
              <span>文房具好きによる、</span><br>
              <span>文房具好きのための、文房具やさん</span>
            </p>
            <div class="header-top-hero-new">
              <p class="header-top-hero-new__title">新着商品</p>
              <ul class="header-top-hero-new__list">
                <?php
                  $posts = get_posts( [
                    'posts_per_page' => 3, // 3件分取得
                    'post_type' => 'item',
                  ] );
                ?>

                <?php foreach ( $posts as $post ) : setup_postdata( $post ) ?>

                  <li class="header-top-hero-new-item">
                    <a href="<?php bloginfo('url'); ?>/items#item<?= esc_attr( get_the_ID() ); ?>">
                      <p class="header-top-hero-new-item__date"><?php the_time('Y/m/d'); ?></p>
                      <p class="header-top-hero-new-item__title"><?php the_title(); ?></p>
                    </a>
                  </li>

                <?php endforeach ?>
                <?php wp_reset_postdata(); ?>
                
              </ul>
            </div>
          </div>
        </div>

      <?php endif ?>

    </header>
