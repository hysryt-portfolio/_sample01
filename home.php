<?php get_header(); ?>

<main>
  <div class="sub-header">
    <div class="sub-header__inner">
      <h2 class="sub-header__title">文房具屋の日々</h2>
      <p class="sub-header__subtitle">
        当店の出来事、最新商品、オススメ商品などをご紹介します。
      </p>
    </div>
  </div>

  <div class="blog-panel">
    <div class="blog-panel__inner">
      <div class="blog-panel__main">
        <div class="blog-articles">

          <?php while ( have_posts() ) : the_post(); ?>
            <article class="blog-article">
              <p class="blog-article__date"><?php the_time("Y/m/d"); ?></p>
              <h3 class="blog-article__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
              <div class="blog-article__info">
                <p class="blog-article__time"><i class="fas fa-clock"></i><?php the_time("H:i"); ?></p>
                <p class="blog-article__category"><i class="fas fa-folder"></i><?php the_category(','); ?></p>
                <?php if ( get_the_tags() ) : ?>
                  <p class="blog-article__tag"><i class="fas fa-tag"></i><?php the_tags(''); ?></p>
                <?php endif ?>
              </div>
              <div class="blog-article__body">
                <?php the_content(); ?>
              </div>
            </article>
          <?php endwhile ?>
        </div>
      </div>
      <div class="blog-panel__side">
        <section class="blog-section-side">
          <h4 class="blog-section-side__title">カテゴリ</h4>
          <div class="blog-section-side__body">
            <?php
              $categories = get_categories();
            ?>

            <ul class="blog-side-list">
              <?php foreach ( $categories as $category ) : ?>
                <?php
                  $name = $category->name;
                  $url = get_category_link( $category->cat_ID );
                ?>
                <li class="blog-side-list__item"><a href="<?= esc_url( $url ); ?>"><?= esc_html( $name ); ?></a></li>
              <?php endforeach ?>
            </ul>

          </div>
        </section>
        <section class="blog-section-side">
          <h4 class="blog-section-side__title">アーカイブ</h4>
          <div class="blog-section-side__body">
            <ul class="blog-side-list">
              <?= wp_get_archives( [
                'format' => 'custom',
                'before' => '<li class="blog-side-list__item">',
                'after' => '</li>'
              ] ); ?>
            </ul>
          </div>
        </section>
      </div>
      <?php if ( is_home() ) : ?>
        <div class="blog-panel__pager">
          <div class="blog-pager">
            <?php
              $pages = paginate_links( [ 'type' => 'array' ] );
            ?>

            <?php foreach ($pages as $pagelink) : ?>
              <?php if (strpos($pagelink, '<span') === 0) : ?>
                <p class="blog-pager__item _current"><?= $pagelink; ?></p>
              <?php else : ?>
                <p class="blog-pager__item"><?= $pagelink; ?></p>
              <?php endif ?>
            <?php endforeach ?>
          </div>
        </div>
      <?php endif ?>
    </div>
  </div>

</main>

<?php get_footer(); ?>