<?php get_header(); ?>

<main>
  <div class="sub-header">
    <div class="sub-header__inner">
      <h2 class="sub-header__title">お問い合わせ</h2>
    </div>
  </div>

  <section class="contact-section">
    <h3 class="contact-section__title">ご意見、ご質問、置いて欲しい商品など<wbr>ございましたらお気軽にお問い合わせください。</h3>
    <div class="contact-table">
      <?php while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
      <?php endwhile ?>
    </div>

  </section>

</main>

<?php get_footer(); ?>