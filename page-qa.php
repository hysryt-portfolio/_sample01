<?php get_header(); ?>

<?php
  $posts = get_posts( [
    'posts_per_page' => 50, // 50件分取得
    'post_type' => 'qa',
  ] );
?>

<main>
  <div class="sub-header">
    <div class="sub-header__inner">
      <h2 class="sub-header__title">Q&amp;A</h2>
      <p class="sub-header__subtitle">
        よくあるご質問
      </p>
    </div>
  </div>
  <div class="qa-list">
    <div class="qa-list__inner">

      <?php foreach ( $posts as $post ) : setup_postdata( $post ) ?>
        <?php
          $answer = get_post_meta( get_the_ID(), QaPostType::META_BOX_ANSWER, true );
        ?>

        <section class="qa-block">
          <div class="qa-question">
            <h3 class="qa-question__title"><?php the_title() ?></h3>
            <p class="qa-question__accordion-switch"><i class="fas fa-angle-down"></i></p>
          </div>
          <div class="qa-answer-wrapper">
            <div class="qa-answer">
              <div class="qa-answer__inner">
                <p><?= esc_html( $answer ); ?></p>
              </div>
            </div>
          </div>
        </section>

      <?php endforeach ?>
      
    </div>
  </div>
</main>

<?php get_footer(); ?>