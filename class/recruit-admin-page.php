<?php

class RecruitAdminPage {
	const META_BOX_ID = 'recruit_meta_box';
	const PROPERTY_NUM = 10;

	private $post_id;

	function __construct() {
		$this->post_id = get_page_by_path('recruit')->ID;

		add_action( 'save_post', [$this, 'save_post']);

		if ( $this->is_recruit_page() ) {
			remove_post_type_support( 'page', 'editor' );
			add_action( 'add_meta_boxes', [$this, 'add_meta_box'] );
		}
	}

	public function add_meta_box() {
		add_meta_box(
			self::META_BOX_ID,
			'採用情報詳細',
			[$this, 'output_meta_box'],
			'page',
            'normal',
            'default'
		);
	}

	public function save_post($post_id) {
		if ($post_id != $this->post_id) {
			return;
		}

		$recruit_properties = [];
		for ($i = 0; $i < self::PROPERTY_NUM; $i++) {
			$recruit_properties[] = [
				"name" => $_POST["recruit-header"][$i],
				"value" => $_POST["recruit-body"][$i]
			];
		}

		update_post_meta( $post_id, "recruit_props", $recruit_properties );
	}

	public function output_meta_box() {
		$recruit_properties = get_post_meta( $this->post_id, "recruit_props", true );

		for ($i = 0; $i < self::PROPERTY_NUM; $i++) {
?>
<div class="recruit-meta-box">
	<div class="recruit-meta-box__row">
		<div class="recruit-meta-box__header-cell">
			<input type="text" name="recruit-header[<?= $i ?>]" value="<?= esc_attr($recruit_properties[$i]["name"]); ?>">
		</div>
		<div class="recruit-meta-box__cell">
			<textarea name="recruit-body[<?= $i ?>]"><?= esc_html($recruit_properties[$i]["value"]); ?></textarea>
		</div>
	</div>
</div>
<?php
		}
	}

	private function is_recruit_page() {
		if ( isset($_GET['post']) && $_GET['post'] == $this->post_id ) {
			return true;
		}

		return false;
	}
}