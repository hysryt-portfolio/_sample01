<?php

class ItemPostType {
    const POST_TYPE = 'item';
    const META_BOX_ITEM_IMAGE = 'meta_box_item_image';
    const META_BOX_ITEM_PRICE = 'meta_box_item_price';
    const META_BOX_ITEM_DESCRIPTION = 'meta_box_item_description';

    private $post_id;

    function __construct() {
        $this->post_id = $_GET["post"] ?? 0;
        
        $this->register_post_type();
        $this->register_taxonomy();

		add_action( 'save_post', [$this, 'save_post']);

        if ( $this->is_item_post_type() ) {
			// 画像アップロード用のスクリプトを追加
            wp_enqueue_media();
            wp_enqueue_script(
                'my-media-upload', get_template_directory_uri() . '/js/script-admin.js'
            );
            
			remove_post_type_support( self::POST_TYPE, 'editor' );
			add_action( 'add_meta_boxes', [$this, 'add_meta_box'] );
        }
    }

    private function register_post_type() {
        register_post_type(
            self::POST_TYPE,
            [
                'labels' => [
                    'name' => '取扱商品',
                    'singluar_name' => '取扱商品'
                ],
                'public' => false,
                'show_ui' => true
            ]
        );
    }

    private function register_taxonomy() {
        register_taxonomy(
            'item_tag',
            self::POST_TYPE,
            [
                'label' => '商品タグ',
            ]
        );
    }

    public function add_meta_box() {
        // 商品画像用メタボックス
		add_meta_box(
			self::META_BOX_ITEM_IMAGE,
			'商品画像',
			[$this, 'output_meta_box_item_image'],
			self::POST_TYPE,
            'normal',
            'default'
        );

        // 商品価格用メタボックス
		add_meta_box(
			self::META_BOX_ITEM_PRICE,
			'商品価格',
			[$this, 'output_meta_box_item_price'],
			self::POST_TYPE,
            'normal',
            'default'
        );
        
        // 商品説明用メタボックス
		add_meta_box(
			self::META_BOX_ITEM_DESCRIPTION,
			'商品説明',
			[$this, 'output_meta_box_item_description'],
			self::POST_TYPE,
            'normal',
            'default'
		);
    }

    public function save_post( $post_id ) {
        $post_type = get_post_type($post_id);
        if ( $post_type !== self::POST_TYPE ) {
            return;
        }

        $item_image_id = $_POST[self::META_BOX_ITEM_IMAGE] ?? "";
        $item_price = $_POST[self::META_BOX_ITEM_PRICE] ?? "";
        $item_description = $_POST[self::META_BOX_ITEM_DESCRIPTION] ?? "";

        update_post_meta( $post_id, self::META_BOX_ITEM_IMAGE, $item_image_id );
        update_post_meta( $post_id, self::META_BOX_ITEM_PRICE, $item_price );
        update_post_meta( $post_id, self::META_BOX_ITEM_DESCRIPTION, $item_description );
    }

    public function output_meta_box_item_image() {
        $image_id = get_post_meta( $this->post_id, self::META_BOX_ITEM_IMAGE, true );
        $image_url = "";
        if ( $image_id !== "" ) {
            $image_url = wp_get_attachment_image_src( $image_id, [300, 300]);
        }
?>
<div class="item-meta-box">
    <div class="item-meta-box__selected-image media-image">
        <?php if ( $image_url != "" && $image_url) : ?>
            <img src="<?= esc_url( $image_url[0] ); ?>" alt="">
        <?php endif ?>
    </div>
    <input type="hidden" class="media-id" name="<?= self::META_BOX_ITEM_IMAGE ?>" value="<?= esc_attr( $image_id ); ?>">
    <button type="button" class="button media-upload-button">画像を選択</button>
</div>
<?php
    }

    public function output_meta_box_item_price() {
        $item_price = get_post_meta( $this->post_id, self::META_BOX_ITEM_PRICE, true);
?>
<div class="item-meta-box">
    <input class="item-meta-box__price" name="<?= self::META_BOX_ITEM_PRICE; ?>" value="<?= esc_attr( $item_price ); ?>">
</div>
<?php
    }

    public function output_meta_box_item_description() {
        $item_description = get_post_meta( $this->post_id, self::META_BOX_ITEM_DESCRIPTION, true );
?>
<div class="item-meta-box">
    <textarea class="item-meta-box__description" name="<?= self::META_BOX_ITEM_DESCRIPTION; ?>"><?= esc_html( $item_description ); ?></textarea>
</div>
<?php
    }

    private function is_item_post_type() {
		if ( isset($_GET['post_type']) && $_GET['post_type'] == self::POST_TYPE ) {
			return true;
        }

        if (isset($_GET['post']) && get_post_type( $_GET['post']) === self::POST_TYPE ) {
            return true;
        }
        
        return false;
    }
}