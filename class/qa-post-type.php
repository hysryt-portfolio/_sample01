<?php

class QaPostType {
    const POST_TYPE = 'qa';
    const META_BOX_ANSWER = 'meta_box_answer';

    private $post_type;

    function __construct() {
        $this->post_id = $_GET["post"] ?? 0;
        
        $this->register_post_type();

		add_action( 'save_post', [$this, 'save_post']);

        if ( $this->is_qa_post_type() ) {
			remove_post_type_support( self::POST_TYPE, 'editor' );
			add_action( 'add_meta_boxes', [$this, 'add_meta_box'] );
        }
    }

    private function register_post_type() {
        register_post_type(
            self::POST_TYPE,
            [
                'labels' => [
                    'name' => 'Q&A',
                    'singluar_name' => 'Q&A'
                ],
                'public' => false,
                'show_ui' => true
            ]
        );
    }

    public function add_meta_box() {
		add_meta_box(
			self::META_BOX_ANSWER,
			'回答',
			[$this, 'output_meta_box'],
			self::POST_TYPE,
            'normal',
            'default'
        );
    }

    public function save_post( $post_id ) {
        $post_type = get_post_type($post_id);
        if ( $post_type !== self::POST_TYPE ) {
            return;
        }

        $answer = $_POST[self::META_BOX_ANSWER] ?? "";

        update_post_meta( $post_id, self::META_BOX_ANSWER, $answer );
    }

    public function output_meta_box() {
        $answer = get_post_meta( $this->post_id, self::META_BOX_ANSWER, true );
?>
<div class="qa-meta-box">
<textarea class="qa-meta-box__answer" name="<?= self::META_BOX_ANSWER; ?>"><?= esc_html( $answer ); ?></textarea>
</div>
<?php
    }

    private function is_qa_post_type() {
		if ( isset($_GET['post_type']) && $_GET['post_type'] == self::POST_TYPE ) {
			return true;
        }

        if (isset($_GET['post']) && get_post_type( $_GET['post']) === self::POST_TYPE ) {
            return true;
        }
        
        return false;
    }
}